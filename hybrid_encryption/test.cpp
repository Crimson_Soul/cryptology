#include "my_AES.h"
#include "choose_operation.h"


int main(const int argc, const char* const argv[]){

	if (sodium_init() == -1){
		return 1;
	}

	my_AES AES = my_AES();
	/** <Operation> [<Operand 1> <Operand 2>] **/

	if (choose_ServerGeneratePartialKey(argc, argv)){
		AES.ServerGeneratePartialKey();
	}

	else if (choose_ClientGeneratePartialKey(argc, argv)){
		AES.ClientGeneratePartialKey();
	}

	else if (choose_ServerGenerateSessionKey(argc, argv)){
		AES.ServerGenerateSessionKey(mpz_class(argv[2]), mpz_class(argv[3]));
	}

	else if (choose_ClientGenerateSessionKey(argc, argv)){
		AES.ClientGenerateSessionKey(mpz_class(argv[2]), mpz_class(argv[3]));
	}

	else if (choose_ServerEncrypt(argc, argv)){
		AES.ServerEncrypt(argv[2], argv[3]);
	}

	else if (choose_ClientDecrypt(argc, argv)){
		AES.ClientDecrypt(argv[2], argv[3]);
	}

	else{
		print_description(argv[0]);
		return 2;
	}

	return 0;
}
