#include <iostream>
#include "my_sign.h"




bool choose_sign(const int& argc, const char* const argv[]){

	if(argc == 5 && !strcmp(argv[1], "Sign")){
			return true;
	}
	return false;
}

bool choose_verify(const int& argc, const char* const argv[]){

	if(argc == 6 && !strcmp(argv[1], "Verify")){
			return true;
	}
	return false;
}

void sign(const char* const argv[]){

	const char* const message = argv[2];
	mpz_class d = mpz_class(argv[3]);
	mpz_class N = mpz_class(argv[4]);
	my_sign::sign(message, d, N);
}

void verify(const char* const argv[]){

	const char* const message = argv[2];
	mpz_class signature = mpz_class(argv[3]);
	mpz_class e = mpz_class(argv[4]);
	mpz_class N = mpz_class(argv[5]);
	my_sign::verify(message, signature, e, N);
}

void print_description(const std::string path){

	std::cout << "Description:\n" << std::endl;
	std::cout << "Signs a message with the private key (d, N)." << std::endl;
	std::cout << "\t\t\t or " << std::endl;
	std::cout << "Verifies a message and signature with the public key (e, N)." << std::endl;
	std::cout << "\n\n\nUsage:\n" << std::endl;
	std::cout << path << " Sign <message <d> <N>" << std::endl;
	std::cout << "\t\t\t or " << std::endl;
	std::cout << path << "  Verify <message> <signature> <e> <N>" << std::endl;
	std::cout << "\n  e: Exponent for encryption" << std::endl;
	std::cout << "  d: Exponent for decryption" << std::endl;
	std::cout << "  N: Modulus" << std::endl;
}




int main(const int argc, const char* const argv[]){

	if (sodium_init() == -1){
		return 1;
	}

	// ./test Sign <message> <d> <N>
	if (choose_sign(argc, argv)){
		sign(argv);
	}

	// ./test Verify <message> <signature> <e> <N>
	else if (choose_verify(argc, argv)){
		verify(argv);
	}

	else{
		print_description(argv[0]);
		return 2;
	}



/*
	// for testing only:
	rsa_key my_key;
	my_key.print_public_key();
	my_key.print_private_key();
*/



	return 0;
}



