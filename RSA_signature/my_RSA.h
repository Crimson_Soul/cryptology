#include <string>
#include <gmpxx.h>


//Anonymous namespace
namespace {

	mpz_class my_powm(mpz_class text, mpz_class exponent, mpz_class mod) {
		mpz_class result;
		mpz_powm(result.get_mpz_t(), text.get_mpz_t(), exponent.get_mpz_t(), mod.get_mpz_t());
		return result;
	}
}


namespace my_rsa {

    std::string encrypt(const std::string& m, const std::string& e, const std::string& N) {
		return my_powm(mpz_class(m), mpz_class(e), mpz_class(N)).get_str();
    }

    std::string decrypt(const std::string& c, const std::string& d, const std::string& N) {
		return my_powm(mpz_class(c), mpz_class(d), mpz_class(N)).get_str();
    }

    mpz_class encrypt(const mpz_class& m, const mpz_class& e, const mpz_class& N) {
		return my_powm(m, e, N);
    }

    mpz_class decrypt(const mpz_class& c, const mpz_class& d, const mpz_class& N) {
		return my_powm(c, d, N);
    }
}



class rsa_key {

private:
	mpz_class public_key;
	mpz_class private_key;
	mpz_class p, q;
	mpz_class N;					// modulo operator
	mpz_class phi_from_N;
	gmp_randstate_t rand_state;		// for random number generation


	void initialize_parameters(){
		N = 0;
		p = 0;
		q = 0;
		phi_from_N = 0;
		public_key = 0;
		private_key = 0;
		gmp_randinit_default(rand_state);
		gmp_randseed_ui(rand_state, time(0));
	}


	mpz_class generate_rand_prime(const size_t& upper_bit_cnt){
		mpz_class tmp_rand;
		mpz_urandomb(tmp_rand.get_mpz_t(), rand_state, upper_bit_cnt);
		mpz_nextprime(tmp_rand.get_mpz_t(), tmp_rand.get_mpz_t());
		return tmp_rand;
	}


	void generate_N(const size_t& lower_bit_cnt, const size_t& upper_bit_cnt){

		while(p == q || mpz_sizeinbase(N.get_mpz_t(), 2) < lower_bit_cnt){
				p = generate_rand_prime(upper_bit_cnt);
				q = generate_rand_prime(upper_bit_cnt);
				N = p * q;
		}
	}


	bool public_key_ok(){

		mpz_class ggT;
		mpz_gcd(ggT.get_mpz_t(), public_key.get_mpz_t(), phi_from_N.get_mpz_t());
		if( (public_key > 1) && (public_key < phi_from_N) && ggT == 1 ){
			return true;
		}
		else{
			return false;
		}
	}


	void generate_keys(){

			generate_N(2048, 2048);	// 2048 bit
			phi_from_N = (p-1) * (q-1);

			// generate public_key
			while( !public_key_ok() ){
				mpz_urandomb(public_key.get_mpz_t(), rand_state, mpz_sizeinbase(phi_from_N.get_mpz_t(), 2));
			}

			// generate private_key
			private_key = my_powm(public_key, mpz_class(-1), phi_from_N);
	}





public:

	rsa_key(){
		initialize_parameters();
		generate_keys();
	}

	~rsa_key(){

	}

	mpz_class get_public_key(){
		return public_key;
	}

	mpz_class get_private_key(){
		return private_key;
	}

	mpz_class get_N(){
		return N;
	}

	void print_public_key(){
		std::cout << "Public key: (" << public_key << ", " << N << ")" << std::endl;
	}

	void print_private_key(){
		std::cout << "Private key: (" << private_key << ", " << N << ")" << std::endl;
	}
};







