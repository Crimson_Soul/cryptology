#include <fstream>
#include "Letter_Accumulator.h"




void count_letters(const std::string& filepath, Letter_Accumulator &key_one,
				Letter_Accumulator &key_two, Letter_Accumulator &key_three){

		std::ifstream myfile_stream (filepath);

		if (myfile_stream.is_open()){

			char letter;
			int i = 1;
			while(myfile_stream.get(letter)){

				if((letter >= 'A') && (letter <= 'Z')){

					switch(i){
						case 1: key_one.increment_letter(letter); break;
						case 2: key_two.increment_letter(letter);	break;
						case 3: key_three.increment_letter(letter); i = 0;
					}
					i++;

				}
			}
			myfile_stream.close();
		}
		else{
			std::cerr << "ERROR: Could not open file!" << std::endl;
		}
}







int main(const int argc, const char * const argv[]){

	if (argc < 2){
		std::cerr << "Description: Analyses most probable key" << std::endl;
		std::cerr << "Usage: " << argv[0] << " <a> " << std::endl;
		std::cerr << "  a: input file path" << std::endl;
		return 1;
	}


	// File path: /home/stud/Documents/Code_Blocks/LB01.ff
	std::string input_file_path = argv[1];


	//testcode:
	Letter_Accumulator keypos_one;
	Letter_Accumulator keypos_two;
	Letter_Accumulator keypos_three;

	count_letters(input_file_path, keypos_one, keypos_two, keypos_three);

	keypos_one.sort_letters();
	keypos_two.sort_letters();
	keypos_three.sort_letters();



	for(int i=0; i<26; i++){

		keypos_one.pop_print_element();
		std::cout << "; ";
		keypos_two.pop_print_element();
		std::cout << "; ";
		keypos_three.pop_print_element();
		std::cout << std::endl;
	}



	return 0;
}





