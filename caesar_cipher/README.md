# Caesar cipher

Cryptanalysis implementing letter frequency on the example of a by Caesar cipher encrypted textfile written in the German language.

Disclaimer: The code in this repository is intended for didactic / academic use only and should under no circumstances be used in security critical applications.
