#include <iostream>
#include <fstream>
#include <string>
#include <vector>



class Decrypter{

private:
	std::string input_file;
	std::string output_file;
	std::string key;
	static const unsigned int offset = 65;		// ASCII val for 'A'



private:

	static int mod(int a, int b){

		a = a % b;
		if( a < 0){ 	// for negative modulo calculation
			a = b + a;
		}
		return a;
	}


	char decrypt_letter(char letter, int keypos){

		int a = (letter - offset) - (key[keypos] - offset);
		return mod(a, 26) + offset;
	}




public:


	void start_decrypting(){

		std::ifstream myifile_stream(input_file);
		std::ofstream myofile_stream(output_file);

		if (myifile_stream.is_open() && myofile_stream.is_open()){

			unsigned int keypos = 0;
			char letter;
			while(myifile_stream.get(letter)){

				if((letter >= 'A') && (letter <= 'Z')){

					myofile_stream << decrypt_letter(letter, keypos);

					if(keypos >= 2){
						keypos = 0;
					}else{
						keypos++;
					}
				}
				else{
					myofile_stream << letter;
				}
			}
			myifile_stream.close();
			myofile_stream.close();
		}
		else{
			std::cerr << "ERROR: Could not open file!" << std::endl;
		}
	}




	static void calc_key_from_cumulated_letters(const std::string &cumulated_letters1){

		std::string tmp_key1;
		for(unsigned int i=0; i<cumulated_letters1.size() ; i++){
			tmp_key1.push_back( ( mod(( (cumulated_letters1[i] - offset) -('E' - offset) ), 26) + offset) );
		}
		std::cout << "Most probable key: " << tmp_key1 << "\n" << std::endl;
	}




public:
	Decrypter(const std::string& input_file, const std::string& output_file, const std::string& key){

		this->input_file = input_file;
		this->output_file = output_file;
		this->key = key;
	}

};

