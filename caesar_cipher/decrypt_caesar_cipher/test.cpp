#include <iostream>
#include <unistd.h>
#include "decrypter.h"



void print_description(const std::string& argv0){
	std::cerr << "Description: Takes input file and generates decrypted output file" << std::endl;
	std::cerr << "Usage: " << argv0 << " <a> <b> <c>" << std::endl;
	std::cerr << "  a: input file path" << std::endl;
	std::cerr << "  b: output file path" << std::endl;
	std::cerr << "  c: key" << std::endl;
}

void print_input(const std::string& input_file_path, const std::string& output_file_path, const std::string& key){
	std::cout << "input file path: " << input_file_path << std::endl;
	std::cout << "output file path: " << output_file_path << std::endl;
	std::cout << "used key: " << key << "\n" << std::endl;
}







int main(const int argc, const char * const argv[]){

	if (argc < 4){
		print_description(argv[0]);
		return 1;
	}


	// File path: /home/stud/Documents/Code_Blocks/LB01.ff     key: "KRY"
	std::string input_file_path = argv[1];
	std::string output_file_path = argv[2];
	std::string key = argv[3];

	print_input(input_file_path, output_file_path, key);


	// TESTCODE:
	Decrypter::calc_key_from_cumulated_letters("OVC");

	Decrypter my_decrypter = Decrypter(input_file_path, output_file_path, key);
	my_decrypter.start_decrypting();





	return 0;
}





